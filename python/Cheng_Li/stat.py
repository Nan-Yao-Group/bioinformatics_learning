#!/usr/bin/env python

chr1 = 'ATATATATAT'
chr2 = 'ATATATATATCGCGCGCGCG'
chr3 = 'ATATATATATCGCGCGCGCGATATATATAT'
chr4 = 'ATATATATATCGCGCGCGCGATATATATATCGCGCGCGCG'
chr5 = 'ATATATATATCGCGCGCGCGATATATATATCGCGCGCGCGATATATATAT'

# generate a dictionary like: {'chr1':20, 'chr2':30}
dict_chr = {}
dict_chr['chr1'] = len(chr1)
dict_chr['chr2'] = len(chr2)
dict_chr['chr3'] = len(chr3)
dict_chr['chr4'] = len(chr4)
dict_chr['chr5'] = len(chr5)

# calculate total length, maximum chromesome, and maximum chromesome length
total_length = 0
max_length = 0
max_chr = ''
for i in dict_chr.keys():
	total_length = total_length + dict_chr.get(i)
	if dict_chr.get(i) > max_length:
		max_length = dict_chr.get(i)
		max_chr = i
	else:
		continue

# calculate gc content
def gc_content(seq):
    SEQ = seq.upper()
    content = (SEQ.count('G') + SEQ.count('C')) / (SEQ.count('G') + SEQ.count('C') + SEQ.count('A') + SEQ.count('T') + SEQ.count('N')) * 100
    
    return round(content, 2)

GC_content = gc_content(chr1 + chr2 + chr3 + chr4)

# print results
print('Total chromesome length: ' + str(total_length))
print('The maximum chromesome: ' + max_chr)
print('The maximum chromesome length: ' + str(max_length))
print('GC content: ' + str(GC_content) + '%')
